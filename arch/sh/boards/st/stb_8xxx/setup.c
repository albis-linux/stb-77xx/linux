/*
 * arch/sh/boards/st/stb_8xxx/setup.c
 *
 * Copyright (C) 2008 STMicroelectronics Limited
 * Author: Stuart Menefy (stuart.menefy@st.com)
 *
 * May be copied or modified under the terms of the GNU General Public
 * License.  See linux/COPYING for more information.
 *
 * STMicroelectronics PDK7105-SDK support.
 */

#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/leds.h>
#include <linux/gpio.h>
#include <linux/tm1668.h>
#include <linux/stm/pio.h>
#include <linux/stm/soc.h>
#include <linux/stm/emi.h>
#include <linux/stm/sysconf.h>
#include <linux/delay.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/physmap.h>
#include <linux/mtd/partitions.h>
#include <linux/mtd/nand.h>
#include <linux/stm/nand.h>
#if 0
#include <linux/spi/spi.h>
#include <linux/spi/flash.h>
#endif
#include <linux/stm/soc_init.h>
#include <linux/phy.h>
#include <asm/irq-ilc.h>
#include <asm/irl.h>
#include <asm/io.h>
#include "../common/common.h"

#define PDK7105_GPIO_PCI_SERR stpio_to_gpio(15, 4)

/*
 * Flash setup depends on boot-device...
 *
 * Jumper settings (board v1.2-011):
 *
 * boot-from-       | NOR                NAND	            SPI
 * ----------------------------------------------------------------------------
 * JE2 (CS routing) | 0 (EMIA->NOR_CS)   1 (EMIA->NAND_CS)  0
 *                  |   (EMIB->NOR_CS)     (EMIB->NOR_CS)     (EMIB->NOR_CS)
 *                  |   (EMIC->NAND_CS)    (EMIC->NOR_CS)     (EMIC->NAND_CS)
 * JE3 (data width) | 0 (16bit)          1 (8bit)           N/A
 * JE5 (mode 15)    | 0 (boot NOR)       1 (boot NAND)	    0 (boot SPI)
 * JE6 (mode 16)    | 0                  0                  1
 * -----------------------------------------------------------------------------
 *
 */

/* this code is based uppon PDK7105-SDK */

/* STB-8xxx use only a single UART and nor RTC/CTS */
#if 0
static int ascs[2] __initdata = { 2, 3 };
#else
static int ascs[1] __initdata = {
	// UART2 without RTS/CTS
	((STASC_FLAG_NORTSCTS << 8) | 2)
};
#endif

static void __init stb_8xxx_setup(char** cmdline_p)
{
	printk("STB-8xxx board initialisation\n");

	stx7105_early_device_init();

	/* STB-8xxx use only a single UART and nor RTC/CTS */
#if 0
	stx7105_configure_asc(ascs, 2, 0);
#else
	stx7105_configure_asc(ascs, 1, 0);
#endif
}

/* unused on STB-8xxx */
#if 0
static struct plat_stm_pwm_data pwm_private_info = {
	.flags		= PLAT_STM_PWM_OUT0,
	.routing	= PWM_OUT0_PIO13_0,
};
#endif

static struct plat_ssc_data ssc_private_info = {
	.capability  =
		ssc0_has(SSC_I2C_CAPABILITY) |
		ssc1_has(SSC_I2C_CAPABILITY) |
		ssc2_has(SSC_I2C_CAPABILITY) |
		ssc3_has(SSC_I2C_CAPABILITY),
	.routing =
		SSC2_SCLK_PIO3_4 | SSC2_MTSR_PIO3_5 |
		SSC3_SCLK_PIO3_6 | SSC3_MTSR_PIO3_7,
};

static struct usb_init_data usb_init[2] __initdata = {
	{
		.oc_en = 1,
		.oc_actlow = 1,
		.oc_pinsel = USB0_OC_PIO4_4,
		.pwr_en = 1,
		.pwr_pinsel = USB0_PWR_PIO4_5,
	}, {
		.oc_en = 1,
		.oc_actlow = 1,
		.oc_pinsel = USB1_OC_PIO4_6,
		.pwr_en = 1,
		.pwr_pinsel = USB1_PWR_PIO4_7,
	}
};

/* unused on STB-8xxx */
#if 0
static struct platform_device stb_8xxx_leds = {
	.name = "leds-gpio",
	.id = 0,
	.dev.platform_data = &(struct gpio_led_platform_data) {
		.num_leds = 2,
		.leds = (struct gpio_led[]) {
			/* The schematics actually describes these PIOs
			 * the other way round, but all tested boards
			 * had the bi-colour LED fitted like below... */
			{
				.name = "RED", /* This is also frontpanel LED */
				.gpio = stpio_to_gpio(7, 0),
				.active_low = 1,
			},
			{
				.name = "GREEN",
				.default_trigger = "heartbeat",
				.gpio = stpio_to_gpio(7, 1),
				.active_low = 1,
			},
		},
	},
};

static struct tm1668_key pdk7105_front_panel_keys[] = {
	{ 0x00001000, KEY_UP, "Up (SWF2)" },
	{ 0x00800000, KEY_DOWN, "Down (SWF7)" },
	{ 0x00008000, KEY_LEFT, "Left (SWF6)" },
	{ 0x00000010, KEY_RIGHT, "Right (SWF5)" },
	{ 0x00000080, KEY_ENTER, "Enter (SWF1)" },
	{ 0x00100000, KEY_ESC, "Escape (SWF4)" },
};

static struct tm1668_character pdk7105_front_panel_characters[] = {
	TM1668_7_SEG_HEX_DIGITS,
	TM1668_7_SEG_HEX_DIGITS_WITH_DOT,
	TM1668_7_SEG_SEGMENTS,
};

static struct platform_device pdk7105_front_panel = {
	.name = "tm1668",
	.id = -1,
	.dev.platform_data = &(struct tm1668_platform_data) {
		.gpio_dio = stpio_to_gpio(11, 2),
		.gpio_sclk = stpio_to_gpio(11, 3),
		.gpio_stb = stpio_to_gpio(11, 4),
		.config = tm1668_config_6_digits_12_segments,

		.keys_num = ARRAY_SIZE(pdk7105_front_panel_keys),
		.keys = pdk7105_front_panel_keys,
		.keys_poll_period = DIV_ROUND_UP(HZ, 5),

		.brightness = 8,
		.characters_num = ARRAY_SIZE(pdk7105_front_panel_characters),
		.characters = pdk7105_front_panel_characters,
		.text = "7105",
	},
};
#endif

/* M.Schenk 2009.11.10 workaround for soft reset */
#if 0
static struct stpio_pin *phy_reset_pin;
#endif

static int stb_8xxx_phy_reset(void* bus)
{
/* M.Schenk 2009.11.10 workaround for soft reset */
#if 0
	stpio_set_pin(phy_reset_pin, 0);
	udelay(100);
	stpio_set_pin(phy_reset_pin, 1);
#else
	struct stpio_pin *phy_reset_pin;

	phy_reset_pin = stpio_request_set_pin(15, 5, "eth_phy_reset",
									      STPIO_OUT, 0);

	if (phy_reset_pin) {
		udelay(1000);
		stpio_set_pin(phy_reset_pin, 1);
		udelay(1000);
		stpio_free_pin(phy_reset_pin);
	}
#endif

	return 1;
}

static struct plat_stmmacphy_data phy_private_data = {
	/* Micrel */
	.bus_id = 0,
	.phy_addr = -1,
	.phy_mask = 0,
	.interface = PHY_INTERFACE_MODE_MII,
	.phy_reset = &stb_8xxx_phy_reset,
};

static struct platform_device stb_8xxx_phy_device = {
	.name		= "stmmacphy",
	.id		= 0,
	.num_resources	= 1,
	.resource	= (struct resource[]) {
		{
			.name	= "phyirq",
			.start	= -1,/*FIXME, should be ILC_EXT_IRQ(6), */
			.end	= -1,
			.flags	= IORESOURCE_IRQ,
		},
	},
	.dev = {
		.platform_data = &phy_private_data,
	}
};

static struct mtd_partition mtd_parts_table[] = {
	{
		.name = "NOR 0",
		.size = 0x00020000,
		.offset = 0x00000000,			/* 0x00000000 ... 0x0001ffff (128KiB) */
	}, {
		.name = "NOR 1",
		.size = 0x00010000,
		.offset = MTDPART_OFS_APPEND,	/* 0x00020000 ... 0x0002ffff (64KiB) */
	}, {
		.name = "NOR 2",
		.size = 0x00010000,
		.offset = MTDPART_OFS_APPEND,	/* 0x00030000 ... 0x0003ffff (64KiB) */
	}, {
		.name = "NOR 3",
		.size = MTDPART_SIZ_FULL,
		.offset = MTDPART_OFS_APPEND,	/* 0x00040000 ... 0x001fffff 		*/
	}
};


static struct physmap_flash_data stb_8xxx_physmap_flash_data = {
	.width		= 2,
	.set_vpp	= NULL,
	.nr_parts	= ARRAY_SIZE(mtd_parts_table),
	.parts		= mtd_parts_table
};

static struct platform_device stb_8xxx_physmap_flash = {
	.name		= "physmap-flash",
	.id		= -1,
	.num_resources	= 1,
	.resource	= (struct resource[]) {
		{
			.start		= 0x00000000,
			.end		= 2*1024*1024 - 1,
			.flags		= IORESOURCE_MEM,
		}
	},
	.dev		= {
		.platform_data	= &stb_8xxx_physmap_flash_data,
	},
};

/* unused on STB-8xxx */
#if 0
/* Configuration for Serial Flash */
static struct mtd_partition serialflash_partitions[] = {
	{
		.name = "SFLASH_1",
		.size = 0x00080000,
		.offset = 0,
	}, {
		.name = "SFLASH_2",
		.size = MTDPART_SIZ_FULL,
		.offset = MTDPART_OFS_NXTBLK,
	},
};

static struct flash_platform_data serialflash_data = {
	.name = "m25p80",
	.parts = serialflash_partitions,
	.nr_parts = ARRAY_SIZE(serialflash_partitions),
	.type = "m25p32",	/* Check device on individual board */
};

static struct spi_board_info spi_serialflash[] =  {
	{
		.modalias       = "m25p80",
		.bus_num        = 0,
		.chip_select    = spi_set_cs(2, 4),
		.max_speed_hz   = 5000000,
		.platform_data  = &serialflash_data,
		.mode           = SPI_MODE_3,
	},
};
#endif

static struct mtd_partition nand_parts[] = {
	{
		.name	= "NAND Recovery Updater",
		.offset	= 0,
		.size 	= 0x01600000					/* 0x00000000 ... 0x015fffff (22MiB) */
	}, {
		.name	= "NAND Updater",
		.offset	= MTDPART_OFS_APPEND,
		.size	= 0x01600000					/* 0x01600000 ... 0x02bfffff (22MiB) */
	}, {
		.name	= "NAND Main",
		.offset	= MTDPART_OFS_APPEND,
		.size	= 0x04000000					/* 0x02C00000 ... 0x06Bfffff (64MiB) */
	}, {
		.name	= "NAND Persistence",
		.offset	= MTDPART_OFS_APPEND,
		.size	= MTDPART_SIZ_FULL				/* 0x06c00000 ... 0x07ffffff (20MiB) */
	},
};

static struct plat_stmnand_data nand_config = {
	/* STM_NAND_EMI data */
	.emi_withinbankoffset	= 0,
	.rbn_port		= -1,
	.rbn_pin		= -1,

	.timing_data = &(struct nand_timing_data) {
		.sig_setup	= 50,		/* times in ns */
		.sig_hold	= 50,
		.CE_deassert	= 0,
		.WE_to_RBn	= 100,
		.wr_on		= 10,
		.wr_off		= 40,
		.rd_on		= 10,
		.rd_off		= 40,
		.chip_delay	= 50,		/* in us */
	},
	.flex_rbn_connected     = 1,
};

/* Platform data for STM_NAND_EMI/FLEX/AFM. (bank# may be updated later) */
static struct platform_device nand_device =
	STM_NAND_DEVICE("stm-nand-afm", 2, &nand_config,
			nand_parts, ARRAY_SIZE(nand_parts), NAND_USE_FLASH_BBT);

/* Albis related platform devices */
static struct platform_device stb_8xxx_kinjector_device = {
	.name		= "kinjector",
	.id		= -1,
	.num_resources	= 0,
};

static struct platform_device stb_8xxx_toolbox_device = {
	.name		= "toolbox",
	.id		= -1,
	.num_resources	= 0,
};

static struct platform_device stb_8xxx_ir_device = {
	.name		= "ir",
	.id		= -1,
	.num_resources	= 0,
};

static struct platform_device stb_8xxx_keypad_device = {
	.name		= "keypad",
	.id		= -1,
	.num_resources	= 0,
};

static struct platform_device stb_8xxx_pt6955_device = {
	.name		= "pt6955",
	.id		= -1,
	.num_resources	= 0,
};

static struct platform_device stb_8xxx_vfd_device = {
	.name		= "vfd",
	.id		= -1,
	.num_resources	= 0,
};

static struct platform_device *stb_8xxx_devices[] __initdata = {
	&stb_8xxx_physmap_flash,
/* unused on STB-8xxx */
#if 0
	&stb_8xxx_leds,
	&pdk7105_front_panel,
#endif
	&stb_8xxx_phy_device,

	/* Albis platform devices */
	&stb_8xxx_kinjector_device,
	&stb_8xxx_toolbox_device,
	&stb_8xxx_ir_device,
	&stb_8xxx_keypad_device,
	&stb_8xxx_pt6955_device,
	&stb_8xxx_vfd_device,
};

/* unused on STB-8xxx */
#if 0
/* Configuration based on Futarque-RC signals train. */
static lirc_scd_t lirc_scd = {
	.code = 0x3FFFC028,
	.nomtime = 0x1f4,
	.noiserecov = 0,
};
#endif

#ifdef CONFIG_PCI
/* PCI configuration */
static struct pci_config_data pdk7105_pci_config = {
	.pci_irq = {
		PCI_PIN_ALTERNATIVE,
		PCI_PIN_UNUSED,
		PCI_PIN_UNUSED,
		PCI_PIN_UNUSED
	},
	.serr_irq = PCI_PIN_UNUSED, /* Modified in device_init() */
	.idsel_lo = 30,
	.idsel_hi = 30,
	.req_gnt = {
		PCI_PIN_DEFAULT,
		PCI_PIN_UNUSED,
		PCI_PIN_UNUSED,
		PCI_PIN_UNUSED
	},
	.pci_clk = 33333333,
	.pci_reset_gpio = stpio_to_gpio(15, 7),
};

int pcibios_map_platform_irq(struct pci_dev *dev, u8 slot, u8 pin)
{
    /* We can use the standard function on this board */
	return stx7105_pcibios_map_platform_irq(&pdk7105_pci_config, pin);
}
#endif

/* fix for broken SSC configuration (SSC2 and SSC3) taken from STAPI 0.18.0 */
static void stb_8xxx_i2c_fixup(void)
{
	void* reg;

	/* map 7105 SYSCFG area */
	reg  = ioremap_nocache((unsigned long)(0xFE001000), 4096);

	if (reg) {
    	/* SYSCFG16 */
    	writel(0x50A00, (u32)reg + 0x0140);
    	wmb();
		/* SYSCFG21 */
    	writel(0x00006C00, (u32)reg + 0x0154);
    	wmb();
    	/* SYSCFG24 */
    	writel(0x00f0, (u32)reg + 0x0164);
    	wmb();

		iounmap(reg);
	}
}

static int __init device_init(void)
{
	u32 bank1_start;
	u32 bank2_start;
	struct sysconf_field *sc;
	u32 boot_mode;

	bank1_start = emi_bank_base(1);
	bank2_start = emi_bank_base(2);

	/* Configure FLASH according to boot device mode pins */
	sc = sysconf_claim(SYS_STA, 1, 15, 16, "boot_mode");
	boot_mode = sysconf_read(sc);
	switch (boot_mode) {
	case 0x0:
		/* Boot-from-NOR */
		pr_info("Configuring FLASH for boot-from-NOR\n");
		stb_8xxx_physmap_flash.resource[0].start = 0x00000000;
		stb_8xxx_physmap_flash.resource[0].end = bank1_start - 1;
		nand_device.id = 2;
		break;
	case 0x1:
		/* Boot-from-NAND */
		pr_info("Configuring FLASH for boot-from-NAND\n");
		stb_8xxx_physmap_flash.resource[0].start = bank1_start;
		stb_8xxx_physmap_flash.resource[0].end = bank2_start - 1;
		nand_device.id = 0;
		break;
	case 0x2:
		/* Boot-from-SPI */
		pr_info("Configuring FLASH for boot-from-SPI\n");
		stb_8xxx_physmap_flash.resource[0].start = bank1_start;
		stb_8xxx_physmap_flash.resource[0].end = bank2_start - 1;
		nand_device.id = 2;
		break;
	}

#ifdef CONFIG_PCI
	/* Setup the PCI_SERR# PIO */
	if (gpio_request(PDK7105_GPIO_PCI_SERR, "PCI_SERR#") == 0) {
		gpio_direction_input(PDK7105_GPIO_PCI_SERR);
		pdk7105_pci_config.serr_irq =
				gpio_to_irq(PDK7105_GPIO_PCI_SERR);
		set_irq_type(pdk7105_pci_config.serr_irq, IRQ_TYPE_LEVEL_LOW);
	} else {
		printk(KERN_WARNING "stb_8xxx: Failed to claim PCI SERR PIO!\n");
	}
	stx7105_configure_pci(&pdk7105_pci_config);
#endif

	stx7105_configure_sata(0);

/* unused on STB-8xxx */
#if 0
	stx7105_configure_pwm(&pwm_private_info);
#endif
	stx7105_configure_ssc(&ssc_private_info);

	/* M.Schenk 2009.10.08 dirty hacking for broken stx7105_configure_ssc() */
	stb_8xxx_i2c_fixup();


	/*
	 * Note that USB port configuration depends on jumper
	 * settings:
	 *		  PORT 0  SW		PORT 1	SW
	 *		+----------------------------------------
	 * OC	normal	|  4[4]	J5A 2-3		 4[6]	J10A 2-3
	 *	alt	| 12[5]	J5A 1-2		14[6]	J10A 1-2
	 * PWR	normal	|  4[5]	J5B 2-3		 4[7]	J10B 2-3
	 *	alt	| 12[6]	J5B 1-2		14[7]	J10B 1-2
	 */

	stx7105_configure_usb(0, &usb_init[0]);
	stx7105_configure_usb(1, &usb_init[1]);

/* M.Schenk 2009.11.10 workaround for soft reset */
#if 0
	phy_reset_pin = stpio_request_set_pin(15, 5, "eth_phy_reset",
					      STPIO_OUT, 1);
#endif
	/* we use internal 25MHz PHY clock from SoC */
	//stx7105_configure_ethernet(0, 0, 0, 0, 0, 0, 0);
	stx7105_configure_ethernet(0, stx7105_ethernet_mii, 0, 0, 0, 0);

/* unused on STB-8xxx */
#if 0
	stx7105_configure_lirc(&lirc_scd);
#endif
	//stx7105_configure_audio_pins(0, 1, 0);
	stx7105_configure_audio_pins(0, 0, 1, 0);

 	/* FLASH_WP is shared by NOR and NAND.  However, since MTD NAND has no
 	   concept of WP/VPP, we must permanently enable it*/
 	stpio_request_set_pin(1, 2, "FLASH_WP", STPIO_OUT, 1);

 	stx7105_configure_nand(&nand_device);
/* unused on STB-8xxx */
#if 0
	/* The serial FLASH device may be driven by PIO15[0-3] (SPIboot
	 * interface) or PIO2[4-6] (SSC0).  The two sets of PIOs are connected
	 * via 3k3 resistor.  Here we are using the SSC.  Therefore to avoid
	 * drive contention, the SPIboot PIOs should be configured as inputs. */
	stpio_request_set_pin(15, 0, "SPIBoot CLK", STPIO_IN, 0);
	stpio_request_set_pin(15, 1, "SPIBoot DOUT", STPIO_IN, 0);
	stpio_request_set_pin(15, 2, "SPIBoot NOTCS", STPIO_IN, 0);
	stpio_request_set_pin(15, 3, "SPIBoot DIN", STPIO_IN, 0);

 	spi_register_board_info(spi_serialflash, ARRAY_SIZE(spi_serialflash));
#endif

	return platform_add_devices(stb_8xxx_devices, ARRAY_SIZE(stb_8xxx_devices));
}
arch_initcall(device_init);

static void __iomem *stb_8xxx_ioport_map(unsigned long port, unsigned int size)
{
	/* Shouldn't be here! */
	BUG();
	return NULL;
}

struct sh_machine_vector mv_stb_8xxx __initmv = {
	.mv_name		= "STB-8xxx",
	.mv_setup		= stb_8xxx_setup,
	.mv_nr_irqs		= NR_IRQS,
	.mv_ioport_map		= stb_8xxx_ioport_map,
	STM_PCI_IO_MACHINE_VEC
};

