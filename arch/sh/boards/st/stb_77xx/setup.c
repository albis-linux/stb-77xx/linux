/*
 * arch/sh/boards/st/stb_77xx/setup.c
 * 
 * based on arch/sh/boards/st/mb448/setup.c
 *
 * Copyright (C) 2005 STMicroelectronics Limited
 * Author: Stuart Menefy (stuart.menefy@st.com)
 *
 * May be copied or modified under the terms of the GNU General Public
 * License.  See linux/COPYING for more information.
 *
 * STMicroelectronics STb7109E Reference board support.
 */

#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/stm/pio.h>
#include <linux/stm/soc.h>
#include <linux/delay.h>
#include <linux/phy.h>
#include <asm/irl.h>

// STB-77xx use only UART2 without RTS/CTS
static int ascs[1] __initdata = { 
	// UART2 without RTS/CTS
	((STASC_FLAG_NORTSCTS << 8) | 2) 
};

void __init stb_77xx_setup(char** cmdline_p)
{
	printk("STB-77xx board initialisation\n");

	stx7100_early_device_init();
	
	// STB-77xx use only UART2 without RTS/CTS
	stb7100_configure_asc(ascs, 1, 0);
}

//static struct plat_ssc_data ssc_private_info = {
//	.capability  =
//		ssc0_has(SSC_I2C_CAPABILITY) |
//		ssc1_has(SSC_SPI_CAPABILITY) |
//		ssc2_has(SSC_I2C_CAPABILITY),
//};
static struct plat_ssc_data ssc_private_info = {
	.capability  =
		ssc0_has(SSC_I2C_CAPABILITY) |
		ssc1_has(SSC_UNCONFIGURED) |
		ssc2_has(SSC_I2C_CAPABILITY),
};

// RTP8201CP MII PHY is on addr 1
static struct plat_stmmacphy_data phy_private_data = {
	.bus_id = 0,
	.phy_addr = 1,
	.phy_mask = 1,
	.interface = PHY_INTERFACE_MODE_MII,
	.phy_reset = NULL,
};

static struct platform_device stb_77xx_phy_device = {
	.name		= "stmmacphy",
	.id		= 0,
	.num_resources	= 1,
	.resource	= (struct resource[]) {
                {
			.name	= "phyirq",
			.start	= 0,
			.end	= 0,
			.flags	= IORESOURCE_IRQ,
		},
	},
	.dev = {
		.platform_data = &phy_private_data,
	 }
};

/* Albis related platform devices */
static struct platform_device stb_77xx_kinjector_device = {
	.name		= "kinjector",
	.id		= -1,
	.num_resources	= 0,
};

static struct platform_device stb_77xx_toolbox_device = {
	.name		= "toolbox",
	.id		= -1,
	.num_resources	= 0,
};

static struct platform_device stb_77xx_ir_device = {
	.name		= "ir",
	.id		= -1,
	.num_resources	= 0,
};

static struct platform_device stb_77xx_keypad_device = {
	.name		= "keypad",
	.id		= -1,
	.num_resources	= 0,
};

static struct platform_device stb_77xx_pt6955_device = {
	.name		= "pt6955",
	.id		= -1,
	.num_resources	= 0,
};

static struct platform_device stb_77xx_vfd_device = {
	.name		= "vfd",
	.id		= -1,
	.num_resources	= 0,
};

static struct platform_device *stb_77xx_devices[] __initdata = {
	&stb_77xx_phy_device,

	/* Albis platform devices */
	&stb_77xx_kinjector_device,
	&stb_77xx_toolbox_device,
	&stb_77xx_ir_device,
	&stb_77xx_keypad_device,
	&stb_77xx_pt6955_device,
	&stb_77xx_vfd_device,
};

static int __init device_init(void)
{
	static struct stpio_pin* boot_progress_led;

	stx7100_configure_sata();
	stx7100_configure_ssc(&ssc_private_info);
	stx7100_configure_usb();
	stx7100_configure_ethernet(0, 0, 0);

	// signal kernel boot up with LED connected to GPIO 3 Pin 4 (CON LED)
	boot_progress_led = stpio_request_set_pin(3, 4, "BOOT", STPIO_OUT, 0);
	stpio_free_pin_leave(boot_progress_led);
	
	return platform_add_devices(stb_77xx_devices,
				    ARRAY_SIZE(stb_77xx_devices));
}

device_initcall(device_init);
