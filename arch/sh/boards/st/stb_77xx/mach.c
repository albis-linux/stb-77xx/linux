/*
 * arch/sh/boards/st/stb_77xx/mach.c
 *
 * based on arch/sh/boards/st/mb448/mach.c
 *
 * Copyright (C) 2005 STMicroelectronics Limited
 * Author: Stuart Menefy (stuart.menefy@st.com)
 *
 * May be copied or modified under the terms of the GNU General Public
 * License.  See linux/COPYING for more information.
 *
 * Machine vector for the STMicroelectronics STb7109E Reference board.
 */

#include <linux/init.h>
#include <linux/irq.h>
#include <asm/system.h>
#include <asm/io.h>
#include <asm/machvec.h>
#include <asm/irq-stb7100.h>

static void __iomem *stb_77xx_ioport_map(unsigned long port, unsigned int size)
{
#ifdef CONFIG_BLK_DEV_ST40IDE
	/*
	 * The IDE driver appears to use memory addresses with IO port
	 * calls. This needs fixing.
	 */
	return (void __iomem *)port;
#endif
	/* However picking somewhere safe isn't as easy as you might think.
	 * I used to use external ROM, but that can cause problems if you are
	 * in the middle of updating Flash. So I'm now using the processor core
	 * version register, which is guaranted to be available, and non-writable.
	 */
	return (void __iomem *)CCN_PVR;
}

static void __init stb_77xx_init_irq(void)
{
	/* enable individual interrupt mode for externals */
	plat_irq_setup_pins(IRQ_MODE_IRQ);
}

void __init stb_77xx_setup(char**);

static struct sh_machine_vector mv_mb448 __initmv = {
	.mv_name		= "mb448",
	.mv_setup		= stb_77xx_setup,
	.mv_nr_irqs		= NR_IRQS,
	.mv_init_irq		= stb_77xx_init_irq,
	.mv_ioport_map		= stb_77xx_ioport_map,
};
