/*
 * -------------------------------------------------------------------------
 * <linux_root>/arch/sh/kernel/cpu/sh4/suspend-stx7105.c
 * -------------------------------------------------------------------------
 * Copyright (C) 2009  STMicroelectronics
 * Author: Francesco M. Virlinzi  <francesco.virlinzi@st.com>
 *
 * May be copied or modified under the terms of the GNU General Public
 * License V.2 ONLY.  See linux/COPYING for more information.
 *
 * ------------------------------------------------------------------------- */

#include <linux/init.h>
#include <linux/suspend.h>
#include <linux/errno.h>
#include <linux/time.h>
#include <linux/delay.h>
#include <linux/irqflags.h>
#include <linux/stm/pm.h>
#include <linux/stm/sysconf.h>
#include <asm/system.h>
#include <asm/io.h>
#include <asm/pm.h>
#include <asm/irq.h>
#include <asm/irq-ilc.h>

#include "soc-stx7105.h"

#define _SYS_STA(x)		(4 * (x) + 0x8)
#define _SYS_CFG(x)		(4 * (x) + 0x100)

/* M.Schenk 2010.03.25 */

/* +12V switch */
#define _IDX_PIO_12V_PIN	(6)
#define _IDX_PIO_12V_OFF	(7)
#define _IDX_PIO_12V_ON		(8)

/* +5V switch */
#define _IDX_PIO_5V_PIN		(9)
#define _IDX_PIO_5V_OFF		(10)
#define _IDX_PIO_5V_ON		(11)

/* *************************
 * STANDBY INSTRUCTION TABLE
 * *************************
 */
static unsigned long stx7105_standby_table[] __cacheline_aligned = {
/* 1. Move all the clock on OSC */
CLK_POKE(CKGA_CLKOPSRC_SWITCH_CFG(0x0), 0x0),
CLK_POKE(CKGA_OSC_DIV_CFG(0x5), 29), /* ic_if_100 @ 1 MHz to be safe for Lirc*/

/* reduces OSC_st40 */
CLK_POKE(CKGA_OSC_DIV_CFG(4), 0x1f),
/* reduces OSC_clk_ic */
CLK_POKE(CKGA_OSC_DIV_CFG(0x0), 0x1f),
/* END. */
_END(),

DATA_LOAD(0x0),
CLK_STORE(CKGA_CLKOPSRC_SWITCH_CFG(0x0)),
DATA_LOAD(0x1),
CLK_STORE(CKGA_OSC_DIV_CFG(0x0)),
DATA_LOAD(0x2),
CLK_STORE(CKGA_OSC_DIV_CFG(5)),
/* END. */
_END()
};

/* *********************
 * MEM INSTRUCTION TABLE
 * *********************
 */
static unsigned long stx7105_mem_table[] __cacheline_aligned = {
/**
 * good time to turn off the unneeded power supplies +5V / +12V
 */

/* PIO1.6 +12V off (HDD/VFD) supply */
DATA_OR_LONG(_IDX_PIO_12V_OFF, _IDX_PIO_12V_PIN),

/* PIO3.1 +5V off */
DATA_OR_LONG(_IDX_PIO_5V_OFF, _IDX_PIO_5V_PIN),

/* 1. Enables the DDR self refresh mode */
SYS_OR_LONG(_SYS_CFG(38), (1 << 20)),
/* waits until the ack bit is zero */
SYS_WHILE_NEQ(_SYS_STA(4), 1, 1),
/* Disable the analogue input buffers of the pads */
SYS_OR_LONG(_SYS_CFG(12), (1 << 10)),
/* Disable the clock output */
SYS_AND_LONG(_SYS_CFG(4), ~(1 << 2)),
/* 1.1 Turn-off the ClockGenD */
SYS_OR_LONG(_SYS_CFG(11), (1 << 12)),
/* wait clock gen lock */
SYS_WHILE_NEQ(_SYS_STA(3), 1, 1),

/* reduces OSC_st40 */
CLK_POKE(CKGA_OSC_DIV_CFG(4), 0x1f),
/* reduces OSC_clk_ic */
CLK_POKE(CKGA_OSC_DIV_CFG(0x0), 0x1f),
/* reduces OSC_clk_ic_if_200 */
CLK_POKE(CKGA_OSC_DIV_CFG(17), 0x1f),
/* 2. Move all the clock on OSC */

CLK_POKE(CKGA_OSC_DIV_CFG(5), 29), /* ic_if_100 @ 1MHz to be safe for Lirc*/

/**
 * M.Schenk 2010.09.02
 * disable any clock from CKGA except 4 (CLK_SH4_ICK), 5 (CLK_IC_IF_100)
 * and 17 (CLK_IC_IF_200).
 */
/*CLK_POKE(CKGA_CLKOPSRC_SWITCH_CFG(0x0), 0),*/
/*CLK_POKE(CKGA_CLKOPSRC_SWITCH_CFG(0x1), 0),*/
CLK_POKE(CKGA_CLKOPSRC_SWITCH_CFG(0x0), 0xfffff0ff),
CLK_POKE(CKGA_CLKOPSRC_SWITCH_CFG(0x1), 0x3),

/* PLLs in power down */
CLK_OR_LONG(CKGA_POWER_CFG, 0x3),
 /* END. */
_END(),

/* Turn-on the PLLs */
CLK_AND_LONG(CKGA_POWER_CFG, ~3),
/* Wait PLLS lock */
CLK_WHILE_NEQ(CKGA_PLL0_CFG, CKGA_PLL0_CFG_LOCK, CKGA_PLL0_CFG_LOCK),
CLK_WHILE_NEQ(CKGA_PLL1_CFG, CKGA_PLL1_CFG_LOCK, CKGA_PLL1_CFG_LOCK),

SYS_AND_LONG(_SYS_CFG(12), ~(1 << 10)),
/* 1. Turn-on the LMI ClocksGenD */
SYS_AND_LONG(_SYS_CFG(11), ~(1 << 12)),
/* Wait LMI ClocksGenD lock */
SYS_WHILE_NEQ(_SYS_STA(3), 1, 0),

/* Enable clock ouput */
SYS_OR_LONG(_SYS_CFG(4), (1 << 2)),
/* Reset LMI Pad logic */
SYS_OR_LONG(_SYS_CFG(11), (1 << 27)),

/* 2. Disables the DDR self refresh mode */
SYS_AND_LONG(_SYS_CFG(38), ~(1 << 20)),
/* waits until the ack bit is zero */
SYS_WHILE_NEQ(_SYS_STA(4), 1, 0),

CLK_POKE(CKGA_PLL0LS_DIV_CFG(4), 0),

/* 3. Restore the previous clocks setting */
DATA_LOAD(0x0),
CLK_STORE(CKGA_CLKOPSRC_SWITCH_CFG(0x0)),
DATA_LOAD(0x1),
CLK_STORE(CKGA_CLKOPSRC_SWITCH_CFG(0x1)),
DATA_LOAD(0x3),
CLK_STORE(CKGA_OSC_DIV_CFG(5)),
DATA_LOAD(0x2),
CLK_STORE(CKGA_OSC_DIV_CFG(0x0)),
DATA_LOAD(0x4),
CLK_STORE(CKGA_OSC_DIV_CFG(17)),

_DELAY(),
_DELAY(),
_DELAY(),

/**
 * good time to turn on the power supplies +5V / +12V
 */

/* PIO3.1 +5V off */
DATA_OR_LONG(_IDX_PIO_5V_ON, _IDX_PIO_5V_PIN),

/* PIO1.6 +12V on (HDD/VFD) supply */
DATA_OR_LONG(_IDX_PIO_12V_ON, _IDX_PIO_12V_PIN),

_END()
};

static unsigned long stx7105_wrt_table[12] __cacheline_aligned;

#define LMI_BASE		0xFE901000
#define   LMI_APPD(bank)	(0x1000 * (bank) + 0x14 + LMI_BASE)
static unsigned long saved_gplmi_appd[0];

#define PIO_PIN_INPUT	4
#define PIO_PIN_OUTPUT	2

#define PIO_BASE(no) (no < 7 ? 0xfd020000 + (no * 0x1000) : 0xfe010000 + ((no - 7) * 0x1000))

static void stx7105_pio_standby_configure_pin(unsigned int port, unsigned int pin, int direction)
{
	iowrite32(1 << pin, PIO_BASE(port) + 0x20 + ((direction & (1 << 0)) ? 0x4 : 0x8));
	iowrite32(1 << pin, PIO_BASE(port) + 0x30 + ((direction & (1 << 1)) ? 0x4 : 0x8));
	iowrite32(1 << pin, PIO_BASE(port) + 0x40 + ((direction & (1 << 2)) ? 0x4 : 0x8));
}

static void stx7105_pio_standby_set_pin(unsigned int port, unsigned int pin, unsigned int value)
{
	iowrite32(1 << pin, PIO_BASE(port) + 0x00 + (value ? 0x4 : 0x8));
}

static unsigned int stx7105_pio_standby_get_pin(unsigned int port, unsigned int pin)
{
	return (ioread32(PIO_BASE(port) + 0x10) & (1 << pin));
}

static unsigned long pio_config[17][3];
static unsigned int pio_pins[5];

static void stx7105_pio_suspend_resume(void)
{
	int portno;
 	
 	for (portno = 0; portno < 17; portno++) {
 		iowrite32(pio_config[portno][0], PIO_BASE(portno) + 0x20);
 		iowrite32(pio_config[portno][1], PIO_BASE(portno) + 0x30);
 		iowrite32(pio_config[portno][2], PIO_BASE(portno) + 0x40);
 	}	
		
	/* PIO 1.0 restore DVB antenna feeding */
	stx7105_pio_standby_set_pin(1, 0, pio_pins[0]);
		
	/* PIO 11.0 restore DVB 5V Loop control */
	stx7105_pio_standby_set_pin(11, 0, pio_pins[1]);

	/* PIO1.2 Flash WP */
	stx7105_pio_standby_set_pin(1, 2, 1);
	stx7105_pio_standby_configure_pin(1, 2, PIO_PIN_OUTPUT);	
}

typedef struct {
	int port;
	int pinstart;
	int pinstop;
	int direction;	
} stx7105_pio_config_t;

static stx7105_pio_config_t stx7105_pio_suspend_config[] = {
	{  0, 0, 7, PIO_PIN_INPUT 	},
	
	{  1, 1, 5, PIO_PIN_INPUT 	},
	{  1, 7, 7, PIO_PIN_INPUT 	},
	
	{  2, 4, 4, PIO_PIN_INPUT 	},
	{  2, 7, 7, PIO_PIN_INPUT 	},	
	
	{  3, 2, 3, PIO_PIN_INPUT 	},

	{  4, 0, 3, PIO_PIN_INPUT 	},	/* RS232 !!! check */

	{  5, 1, 7, PIO_PIN_INPUT 	},
	
	{  6, 0, 7, PIO_PIN_INPUT 	},
	
	{  7, 0, 3, PIO_PIN_INPUT 	},
	
	{  9, 7, 7, PIO_PIN_INPUT 	}, /* HDMI HPD !!! check */
		
	{ 10, 0, 7, PIO_PIN_INPUT 	},
	
	{ 11, 1, 7, PIO_PIN_INPUT 	},
	
	{ 12, 0, 7, PIO_PIN_INPUT 	},
	
	{ 13, 0, 7, PIO_PIN_INPUT 	},
	
	{ 14, 0, 7, PIO_PIN_INPUT 	},
	
	/**
	 * Reminder, do not set PCI_PME to input HI-Z ! This blocks the resume
	 * if a video was started before suspend to RAM
	 */
	{ 15, 0, 4, PIO_PIN_INPUT 	},
	{ 15, 7, 7, PIO_PIN_INPUT 	},

	{ 16, 0, 7, PIO_PIN_INPUT 	},
};
 
static void stx7105_pio_suspend_prepare(void)
{
	int entry;
	int portno;

 	/* safe current PIO configuration */
 	for (portno = 0; portno < 17; portno++) {
 		pio_config[portno][0] = ioread32(PIO_BASE(portno) + 0x20);
 		pio_config[portno][1] = ioread32(PIO_BASE(portno) + 0x30);
 		pio_config[portno][2] = ioread32(PIO_BASE(portno) + 0x40);
 	}
 	 	
 	/* configure any unused PIO to input (HI-Z) */
 	for (entry = 0; entry < (sizeof(stx7105_pio_suspend_config) / sizeof(stx7105_pio_suspend_config[0])); entry++) {
 		stx7105_pio_config_t* cfg = &stx7105_pio_suspend_config[entry];
 		
 		for (portno = cfg->pinstart; portno <= cfg->pinstop; portno++)
 			stx7105_pio_standby_configure_pin(cfg->port, portno, cfg->direction);
 	}	

	/* PIO 1.0 DVB antenna feeding */
	if (stx7105_pio_standby_get_pin(1, 0))
		pio_pins[0] = 1;	
	else 
		pio_pins[0] = 0;
		
	/* PIO 1.0 disable DVB antenna feeding */
	stx7105_pio_standby_set_pin(1, 0, 0);
	stx7105_pio_standby_configure_pin(1, 0, PIO_PIN_OUTPUT);
	
	/* PIO 11.0 DVB 5V Loop control */
	if (stx7105_pio_standby_get_pin(11, 0))
		pio_pins[1] = 1;	
	else 
		pio_pins[1] = 0;

	/* DVB 5V Loop control */
	stx7105_pio_standby_set_pin(11, 0, 0);
	stx7105_pio_standby_configure_pin(11, 0, PIO_PIN_OUTPUT);
}

unsigned long ckgb_config;

static void stx7105_ckgb_suspend_resume(void)
{
	unsigned long value;
	
	/* unlock CKGB clocks register */
	iowrite32(0xc0de, 0xfe000010);

	/* restore all CKGB clocks (reset bits [0-12]) */
	iowrite32(ckgb_config, 0xfe0000b0);
	
	/**
	 * power up FS0 analog and digital parts
	 */
	 
	/* analog power up FS0_NPDA */
	value = ioread32(0xfe000014);
	value |= (1 << 4);
	iowrite32(value, 0xfe000014);	

	/* digital up down FS0_NSB bits [3:0] */
	value = ioread32(0xfe000058);
	value |= 0xf;
	iowrite32(value, 0xfe000058);	

	/**
	 * M.Schenk 2010.09.02
	 * CKGB FS1 is used for generting the LPC clock.
	 * If we like to wakeup at a given time we need the LPC and
	 * of course the clock.
	 */
#if 0
	/**
	 * power up FS1 analog and digital parts
	 */
	 	
	/* analog power up FS1_NPDA */
	value = ioread32(0xfe00005c);
	value |= (1 << 4);
	iowrite32(value, 0xfe00005c);	
	
	/* digital power up FS1_NSB bits [3:0] */
	value = ioread32(0xfe0000a0);
	value |= 0xf;
	iowrite32(value, 0xfe0000a0);	
#endif	
	
	/* lock CKGB clocks register */
	iowrite32(0xc1a0, 0xfe000010);			
}

static void stx7105_ckgb_suspend_prepare(void)
{
	unsigned long value;
	
	/* unlock CKGB clocks register */
	iowrite32(0xc0de, 0xfe000010);

	/* turn off all CKGB clocks (reset bits [0-12]) */
	ckgb_config = ioread32(0xfe0000b0);
	iowrite32(ckgb_config & ~0x1fff, 0xfe0000b0);
	
	/**
	 * power down FS0 analog and digital parts
	 */
	 
	/* analog power down FS0_NPDA */
	value = ioread32(0xfe000014);
	value &= ~(1 << 4);
	iowrite32(value, 0xfe000014);	

	/* digital power down FS0_NSB bits [3:0] */
	value = ioread32(0xfe000058);
	value &= ~0xf;
	iowrite32(value, 0xfe000058);	

	/**
	 * M.Schenk 2010.09.02
	 * CKGB FS1 is used for generting the LPC clock.
	 * If we like to wakeup at a given time we need the LPC and
	 * of course the clock.
	 */
#if 0
	/**
	 * power down FS1 analog and digital parts
	 */
	 	
	/* analog power down FS1_NPDA */
	value = ioread32(0xfe00005c);
	value &= ~(1 << 4);
	iowrite32(value, 0xfe00005c);	
	
	/* digital power down FS1_NSB bits [3:0] */
	value = ioread32(0xfe0000a0);
	value &= ~0xf;
	iowrite32(value, 0xfe0000a0);	
#endif
	
	/* lock CKGB clocks register */
	iowrite32(0xc1a0, 0xfe000010);
}

static unsigned long ips_config[17];

static void stx7105_ips_suspend_resume(void)
{
	int cnt;
	unsigned long value;
	
	/* power on audio DACs (set bits 3,5 & 6 of AUD_ADAC_CTRL) */
	iowrite32(ips_config[0] , 0xfe210100);
	
	/* power on audio FS (reset bits 14-10 of AUD_FSYN_CFG) */
	iowrite32(ips_config[1] , 0xfe210000);

	/* enable SD & HD DACs (set bits 5 - 0 of SYS_CONFIG3) */
	iowrite32(ips_config[2], 0xfe00110c);

	/* power on HDMI */
	iowrite32(ips_config[3], 0xfe001108);
	
	/* power up HDMI PLL */	
	if (ips_config[4]) {
		value = ioread32(0xfe00110c);	
		
		value |= (1 << 12);
		iowrite32(value, 0xfe00110c);

		/* waits until the ack bit is one */	
		for (cnt = 0; cnt < 10; cnt++) {
			/* query SYS_STATUS9 HDMI_PLL_LOCK bit */
			value = ioread32(0xfe00102c);

			if ((value & 0x1) == 0x1)
				break;
				
			msleep(10);
		}
	}

	/**
	 * power on EMI, PCI, key scan, USB 0 & 1, SATA
	 */	

	/* restore SYSTEM_CONFIG32 from suspend prepare value */
	iowrite32(ips_config[6], 0xfe001180);	

	/* waits for ack bits */
	for (cnt = 0; cnt < 10; cnt++) {
		value = ioread32(0xfe001044);

		if ((value & 0xbe) == ips_config[5])
			break;
			
		msleep(10);
	}	

	/* start USB clocks ??? */
	iowrite32(ips_config[7], 0xfe0011a0);	

	/* start thermal sensor */
	iowrite32(ips_config[8], 0xfe0011a4);		
}

static void stx7105_ips_suspend_prepare(void)
{
	int cnt;
	unsigned long value;

	/* power down audio DACs (reset bits 3,5 & 6 of AUD_ADAC_CTRL) */
	ips_config[0] = ioread32(0xfe210100);
	iowrite32(ips_config[0] & ~0x68, 0xfe210100);
	
	/* power down audio FS (reset bits 14-10 of AUD_FSYN_CFG) */
	ips_config[1] = ioread32(0xfe210000);
	iowrite32(ips_config[1] & ~0x7c00, 0xfe210000);
	
	/* disable SD & HD DACs (set bits 5 - 0 of SYS_CONFIG3) */
	ips_config[2] = ioread32(0xfe00110c);
	iowrite32(ips_config[2] | 0x3f, 0xfe00110c);

	/* power down HDMI */
	ips_config[3] = ioread32(0xfe001108);
	iowrite32(ips_config[3] | (1 << 26), 0xfe001108);

	/* power down HDMI PLL */	
	ips_config[4] = 0;	
	
	value = ioread32(0xfe00110c);
	
	/* check if HDMI PLL need to power off */
	if (value & (1 << 12)) {
		ips_config[4] = 1;	

		value &= ~(1 << 12);
		iowrite32(value, 0xfe00110c);

		/* waits until the ack bit is zero */		
		for (cnt = 0; cnt < 10; cnt++) {
			/* query SYS_STATUS9 HDMI_PLL_LOCK bit */
			value = ioread32(0xfe00102c);

			if ((value & 0x1) == 0)
				break;
				
			msleep(10);			
		}
	}
	
	/**
	 * power down EMI, PCI, key scan, USB 0 & 1, SATA
	 */
	 
	/* store SYS_STATUS15 power down ack used for resume */
	ips_config[5] = ioread32(0xfe001044) & 0xbe;
	
	/* read current SYSTEM_CONFIG32 register */
	value = ioread32(0xfe001180);
	ips_config[6] = value;
	
	/* SATA HC */
	if ((value & (1 << 11)) == 0x0)
		value |= (1 << 11);
	
	/* SATA PHY */	
	if ((value & (1 << 9)) == 0x0)
		value |= (1 << 9);			
	
	/* Reserved ??? */
	if ((value & (1 << 8)) == 0x0)
		value |= (1 << 8);	
	
	/* USB2/1 PHY */
	if ((value & (1 << 7)) == 0x0)
		value |= (1 << 7);	
	if ((value & (1 << 6)) == 0x0)
		value |= (1 << 6);	

	/* USB2/1 HC */
	if ((value & (1 << 5)) == 0x0)
		value |= (1 << 5);	
	if ((value & (1 << 4)) == 0x0)
		value |= (1 << 4);	

	/* KEYSCAN */
	if ((value & (1 << 3)) == 0x0)
		value |= (1 << 3);	

	/* PCI */
	if ((value & (1 << 2)) == 0x0)
		value |= (1 << 2);	

	/* EMI */
	if ((value & (1 << 1)) == 0x0)
		value |= (1 << 1);	
		
	/* Reserved ??? */
	if ((value & (1 << 0)) == 0x0)
		value |= (1 << 0);

	/* power down any currently active IP blocks on SoC */
	iowrite32(value , 0xfe001180);

	/* waits for ack bits*/	
	for (cnt = 0; cnt < 10; cnt++) {
		value = ioread32(0xfe001044);

		if ((value & 0xbe) == 0xbe)
			break;
			
		msleep(10);		
	}

	/* stop USB clocks */
	ips_config[7] = ioread32(0xfe0011a0);
	iowrite32(ips_config[7] | 0xc, 0xfe0011a0);
	
	/* power down thermal sensor */
	ips_config[8] = ioread32(0xfe0011a4);
	iowrite32(ips_config[8] & ~(1 << 4), 0xfe0011a4);
}
static unsigned long a344;

static int stx7105_suspend_prepare(suspend_state_t state)
{
	if (state == PM_SUSPEND_STANDBY) {
		stx7105_wrt_table[0] = /* swith config */
		   ioread32(CLOCKGENA_BASE_ADDR + CKGA_CLKOPSRC_SWITCH_CFG(0));
		stx7105_wrt_table[1] = /* clk_STNoc */
		   ioread32(CLOCKGENA_BASE_ADDR + CKGA_OSC_DIV_CFG(0));
		stx7105_wrt_table[2] = /* clk_ic_if_100 */
		   ioread32(CLOCKGENA_BASE_ADDR + CKGA_OSC_DIV_CFG(5));
	} else {
		stx7105_ips_suspend_prepare();


		/* PIO 1.6 HDD Power off (+12V) */
		stx7105_wrt_table[_IDX_PIO_12V_PIN] = (1 << 6);
			
		/* PIO 1.6 clear reg */
		stx7105_wrt_table[_IDX_PIO_12V_OFF] = 0xfd021000 + 0x8;
		
		/* PIO 1.6 set reg */
		stx7105_wrt_table[_IDX_PIO_12V_ON] = 0xfd021000 + 0x4;
	
		/* PIO 3.1 STANDBY (+5V) */
		stx7105_wrt_table[_IDX_PIO_5V_PIN] = (1 << 1);
		
		/* PIO 3.1 clear reg */
		stx7105_wrt_table[_IDX_PIO_5V_OFF] = 0xfd023000 + 0x8;
		
		/* PIO 3.1 set reg */
		stx7105_wrt_table[_IDX_PIO_5V_ON] = 0xfd023000 + 0x4;

		/* shutdown CLKGB on suspend */
		stx7105_ckgb_suspend_prepare();

		stx7105_wrt_table[0] = /* swith config */
		   ioread32(CLOCKGENA_BASE_ADDR + CKGA_CLKOPSRC_SWITCH_CFG(0));
		stx7105_wrt_table[1] = /* swith config 1 */
		   ioread32(CLOCKGENA_BASE_ADDR + CKGA_CLKOPSRC_SWITCH_CFG(1));
		stx7105_wrt_table[2] = /* clk_STNoc */
		   ioread32(CLOCKGENA_BASE_ADDR + CKGA_OSC_DIV_CFG(0));
		stx7105_wrt_table[3] = /* clk_ic_if_100 */
		   ioread32(CLOCKGENA_BASE_ADDR + CKGA_OSC_DIV_CFG(5));
		stx7105_wrt_table[4] = /* clk_ic_if_200 */
		   ioread32(CLOCKGENA_BASE_ADDR + CKGA_OSC_DIV_CFG(17));

		stx7105_pio_suspend_prepare();

		/**
		 * Code from STAPI OS21 suspend which is needed for reliable wakeup.....
		 * Sadly to say that i don't own register documentation so this are just
		 * magic IRQ peeks and pokes.
		 * ./STAPI_SDK-REL_0.24.0/stapisdk/apilib/src/stpower/src/cache/cache_execution.c
		 */
		{
			#define INTC2BaseAddress        0xFE001000
			#define ILC_BASE_ADDRESS        0xFD000000
			
			unsigned long tmp;
			
			/*INTC2*/
			a344 = ioread32(INTC2BaseAddress + 0x344);
			/* *(int*)(INTC2BaseAddress + 0x304) = 0xAC007706;   */ /*Priority*/
			// *(int*)(INTC2BaseAddress + 0x364) = 0x00001000 ;  /*Mask*/
			
			iowrite32(0x00001000 , INTC2BaseAddress + 0x364);
			
			/*ILC3*/
			//*(int*)(ILC_BASE_ADDRESS + 0xA24) = 0x4;    /*ILC_EXT_MODE4*/
			iowrite32(0x4 , ILC_BASE_ADDRESS + 0xA24);
			
			// *(int*)(ILC_BASE_ADDRESS + 0xA20) = 0x8000; /*ILC_EXT_PRIORITY4*/
			iowrite32(0x8000 , ILC_BASE_ADDRESS + 0xA20);
			
			// *(int*)(ILC_BASE_ADDRESS + 0x608) |= (1<<4);/*ILC_WAKEUP_ENABLE2 */
			
			tmp = ioread32(ILC_BASE_ADDRESS + 0x608);
			iowrite32(tmp | (1<<4) , ILC_BASE_ADDRESS + 0x608);
		}
	}

	/* save the current APPD setting*/
	saved_gplmi_appd[0] = ioread32(LMI_APPD(0));
	/* disable the APPD */
	iowrite32(0x0, LMI_APPD(0));
	
	return 0;
}

static int stx7105_suspend_finish(suspend_state_t state)
{
	/* restore the APPD */
	iowrite32(saved_gplmi_appd[0], LMI_APPD(0));
	
	if (state == PM_SUSPEND_STANDBY) {
		/* TODO ..... */
	}
	else {
/* If enable this code we have still problems to come back from suspend to RAM */
#if 0
		/**
		 * Code from STAPI OS21 suspend which is needed for reliable wakeup.....
		 * Sadly to say that i don't own register documentation so this are just
		 * magic IRQ peeks and pokes.
		 * ./STAPI_SDK-REL_0.24.0/stapisdk/apilib/src/stpower/src/cache/cache_execution.c
		 */
	 
		//    *(int*)(INTC2BaseAddress + 0x344) = a344;
		iowrite32(a344 , INTC2BaseAddress + 0x344);
		
		//     *(int*)(ILC_BASE_ADDRESS + 0x608) = 0;/*ILC_WAKEUP_ENABLE2 */
		iowrite32(0 , ILC_BASE_ADDRESS + 0x608);
#endif

		/* suspend to RAM */	
		stx7105_pio_suspend_resume();

		stx7105_ckgb_suspend_resume();

		stx7105_ips_suspend_resume();
	}

	return 0;	
}

static unsigned long stx7105_iomem[] __cacheline_aligned = {
		stx7105_wrt_table,
		CLOCKGENA_BASE_ADDR,
		0,
		SYSCONF_BASE_ADDR
};

static int stx7105_evt_to_irq(unsigned long evt)
{
	
	printk("stx7105_evt_to_irq 0x%lx\n", evt);
	
	return ((evt < 0x400) ? ilc2irq(evt) : evt2irq(evt));
}

static struct sh4_suspend_t st40data __cacheline_aligned = {
	.iobase = stx7105_iomem,
	.ops.prepare = stx7105_suspend_prepare,
	.ops.finish = stx7105_suspend_finish,
	.evt_to_irq = stx7105_evt_to_irq,

	.stby_tbl = (unsigned long)stx7105_standby_table,
	.stby_size = DIV_ROUND_UP(ARRAY_SIZE(stx7105_standby_table) *
			sizeof(long), L1_CACHE_BYTES),

	.mem_tbl = (unsigned long)stx7105_mem_table,
	.mem_size = DIV_ROUND_UP(ARRAY_SIZE(stx7105_mem_table) * sizeof(long),
			L1_CACHE_BYTES),
	.wrt_tbl = (unsigned long)stx7105_wrt_table,
	.wrt_size = DIV_ROUND_UP(ARRAY_SIZE(stx7105_wrt_table) * sizeof(long),
			L1_CACHE_BYTES),
};

static int __init suspend_platform_setup(void)
{
	struct sysconf_field* sc;
	sc = sysconf_claim(SYS_STA, 4, 0, 0, "pm");
	sc = sysconf_claim(SYS_STA, 3, 0, 0, "pm");
	sc = sysconf_claim(SYS_CFG, 4, 2, 2, "pm");
	sc = sysconf_claim(SYS_CFG, 11, 12, 12, "pm");
	sc = sysconf_claim(SYS_CFG, 11, 27, 27, "pm");
	sc = sysconf_claim(SYS_CFG, 12, 10, 10, "pm");
	sc = sysconf_claim(SYS_CFG, 38, 20, 20, "pm");

	return sh4_suspend_register(&st40data);
}

late_initcall(suspend_platform_setup);
