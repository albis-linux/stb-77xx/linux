// STB77xx NAND Flash Driver

#include <linux/slab.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/ioport.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/nand.h>
#include <linux/stm/pio.h>
#include <asm/io.h>

#ifdef CONFIG_MTD_PARTITIONS
#include <linux/mtd/partitions.h>
#endif

#define STB77XX_NUM_PARTITIONS		2
#ifdef CONFIG_SUPPORTS_32BIT
#define STB77XX_PHYSICAL_NAND_ADDR	0x01000000
#else
#define STB77XX_PHYSICAL_NAND_ADDR	0xa1000000
#endif
#define STB77XX_NAND_IOREMAP_SIZE	0x1000
#define SZ_1M 						0x00100000

#define NAND_WP_ON		0
#define NAND_WP_OFF		1
#define NAND_CL_SET		1
#define NAND_CL_CLR		0
#define NAND_AL_SET		1
#define NAND_AL_CLR		0

// READY/BUSY is on GPIO port 4 pin 4
#define NAND_RB_PORT	4
#define NAND_RB_PIN		4

// COMMAND LATCH is on GPIO port 4 pin 5
#define NAND_CL_PORT	4
#define NAND_CL_PIN		5

// ADDRESS LATCH is on GPIO port 4 pin 6
#define NAND_AL_PORT	4
#define NAND_AL_PIN		6

// WRITE PROTECT is on GPIO port 4 pin 7
#define NAND_WP_PORT	4
#define NAND_WP_PIN		7

// MTD structure for STB71xx board
static struct mtd_info* stb_77xx_nand_mtd = NULL;
void __iomem* stb_77xx_nand_baseaddr;

static struct stpio_pin *stb_77xx_nand_rb = NULL;
static struct stpio_pin *stb_77xx_nand_cl = NULL;
static struct stpio_pin *stb_77xx_nand_al = NULL;
static struct stpio_pin *stb_77xx_nand_wp = NULL;
          
// Define partitions for flash devices
static struct mtd_partition stb_77xx_nand_partitions[] = {
	{ 
		.name =	"NAND Main Rootdisk",
		.offset = 0,
		.size = (27 * SZ_1M)
	},
	{ 
		.name =	"NAND Persistence",
		.offset = MTDPART_OFS_APPEND,
		.size = (5 * SZ_1M)
	}
};

////////////////////////////////////////////////////////////////////////////////
// configure GPIO pins used to drive NAND flash
//
// PARAMETERS
//	NONE
//	
//	RETURN
// 		NONE
////////////////////////////////////////////////////////////////////////////////
static void stb_77xx_nand_init_pio(void)
{
	// request READY/BUSY
	stb_77xx_nand_rb = stpio_request_pin(NAND_RB_PORT, NAND_RB_PIN, 
										"NAND_RB", STPIO_BIDIR_Z1);

	// request COMMAND LATCH
	stb_77xx_nand_cl = stpio_request_pin(NAND_CL_PORT, NAND_CL_PIN, 
										"NAND_CL", STPIO_OUT);								

	// request ADDRESS LATCH
	stb_77xx_nand_al = stpio_request_pin(NAND_AL_PORT, NAND_AL_PIN, 
										"NAND_AL", STPIO_OUT);	
	
	// request WRITE PROTECT
	stb_77xx_nand_wp = stpio_request_pin(NAND_WP_PORT, NAND_WP_PIN, 
										"NAND_WP", STPIO_OUT);	
	// disable WP
	stpio_set_pin(stb_77xx_nand_wp, NAND_WP_OFF);
} // end of stb_77xx_nand_init_pio

////////////////////////////////////////////////////////////////////////////////
// free aquired GPIO pins used to drive NAND flash
//
// PARAMETERS
//	NONE
//	
//	RETURN
// 		NONE
////////////////////////////////////////////////////////////////////////////////
static void stb_77xx_nand_deinit_pio(void)
{
	stpio_free_pin(stb_77xx_nand_rb);
	stpio_free_pin(stb_77xx_nand_cl);	
	stpio_free_pin(stb_77xx_nand_al);	
	stpio_free_pin(stb_77xx_nand_wp);		
} // end of stb_77xx_nand_deinit_pio

////////////////////////////////////////////////////////////////////////////////
// check if NAND device is ready
//
// PARAMETERS
// 	mtd [IN] struct mtd_info*
//		ptr to struct mtd_info
//	
//	RETURN
// 		int (0 means device is not ready)
////////////////////////////////////////////////////////////////////////////////
static int stb_77xx_nand_dev_ready(struct mtd_info* mtd)
{
	return stpio_get_pin(stb_77xx_nand_rb);
} // end of stb_77xx_nand_dev_ready

////////////////////////////////////////////////////////////////////////////////
// hardware specific access to control-lines
//
// PARAMETERS
// 	mtd [IN] struct mtd_info*
//		ptr to struct mtd_info
// 	cmd [IN] int
//		command as int
// 	ctrl [IN] unsigned int
//		control command
//	
//	RETURN
// 		NONE
////////////////////////////////////////////////////////////////////////////////
static void stb_77xx_nand_cmd_ctrl(struct mtd_info *mtd, int cmd, unsigned int ctrl)
{
	struct nand_chip *chip = mtd->priv;	
	
	if(ctrl & NAND_CTRL_CHANGE) {
		if(ctrl & NAND_ALE) {
			stpio_set_pin(stb_77xx_nand_al, NAND_AL_SET);
		}
		else {
			stpio_set_pin(stb_77xx_nand_al, NAND_AL_CLR);
		}

		if(ctrl & NAND_CLE) {
			stpio_set_pin(stb_77xx_nand_cl, NAND_CL_SET);
		}
		else {
			stpio_set_pin(stb_77xx_nand_cl, NAND_CL_CLR);
		}
	}	

	if(cmd != NAND_CMD_NONE) {
		writeb(cmd, chip->IO_ADDR_W);	
	}
} // end of stb_77xx_nand_cmd_ctrl

#ifdef CONFIG_MTD_PARTITIONS
const char *part_probes[] = { "cmdlinepart", NULL };
#endif

////////////////////////////////////////////////////////////////////////////////
// driver initialization routine
//
// PARAMETERS
//	NONE
//	
//	RETURN
// 		int (0 otherwise error code)
////////////////////////////////////////////////////////////////////////////////
int __init stb_77xx_nand_init(void)
{
	struct nand_chip* this;
	const char* part_type = 0;
	int mtd_parts_nb = 0;
	struct mtd_partition* mtd_parts = 0;
	int err = 0;

	// Allocate memory for MTD device structure and private data
	stb_77xx_nand_mtd = kmalloc(sizeof(struct mtd_info) + sizeof (struct nand_chip),
							   GFP_KERNEL);
	if(!stb_77xx_nand_mtd) {
		printk (KERN_WARNING "Unable to allocate STB71xx NAND MTD device structure.\n");
		return -ENOMEM;
	}
	
	// map physical adress
	stb_77xx_nand_baseaddr = ioremap(STB77XX_PHYSICAL_NAND_ADDR, STB77XX_NAND_IOREMAP_SIZE);
	if(!stb_77xx_nand_baseaddr){
		printk("ioremap to access simonng NAND chip failed\n");
		kfree(stb_77xx_nand_mtd);
		return -EIO;
	}

	// init pio
	stb_77xx_nand_init_pio();

	// Get pointer to private data 
	this = (struct nand_chip*)(&stb_77xx_nand_mtd[1]);

	// Initialize structures
	memset((char*)stb_77xx_nand_mtd, 0, sizeof(struct mtd_info));
	memset((char*)this, 0, sizeof(struct nand_chip));

	// Link the private data with the MTD structure
	stb_77xx_nand_mtd->priv = this;

	// Set address of NAND IO lines
	this->IO_ADDR_R = stb_77xx_nand_baseaddr;
	this->IO_ADDR_W = stb_77xx_nand_baseaddr;
	this->cmd_ctrl = stb_77xx_nand_cmd_ctrl;
	this->dev_ready = stb_77xx_nand_dev_ready;
	
	// 30 us command delay time
	this->chip_delay = 30;		
	this->ecc.mode = NAND_ECC_SOFT;

#if 0
	this->options = NAND_USE_FLASH_BBT | NAND_BBT_SCANEMPTY | NAND_BBT_LASTBLOCK | NAND_BBT_CREATE;
#endif
	
	// Scan to find existence of the device
	err = nand_scan(stb_77xx_nand_mtd, 1);
	if (err) {
		iounmap(stb_77xx_nand_baseaddr);
		kfree(stb_77xx_nand_mtd);
		return err;
	}
	
#ifdef CONFIG_MTD_PARTITIONS
	stb_77xx_nand_mtd->name = "STB77xx NAND Flash";
	mtd_parts_nb = parse_mtd_partitions(stb_77xx_nand_mtd, part_probes, &mtd_parts, 0);
	if (mtd_parts_nb > 0)
	  part_type = "command line";
	else
	  mtd_parts_nb = 0;
#endif
	if(mtd_parts_nb == 0) {
		mtd_parts = stb_77xx_nand_partitions;
		mtd_parts_nb = STB77XX_NUM_PARTITIONS;
		part_type = "static";
	}

	// Register the partitions
	printk(KERN_NOTICE "Using %s partition definition\n", part_type);
	add_mtd_partitions(stb_77xx_nand_mtd, mtd_parts, mtd_parts_nb);

	return err;
} // end of stb_77xx_nand_init

module_init(stb_77xx_nand_init);

////////////////////////////////////////////////////////////////////////////////
// Clean up routine
//
// PARAMETERS
//	NONE
//	
//	RETURN
// 		NONE
////////////////////////////////////////////////////////////////////////////////
static void __exit stb_77xx_nand_cleanup(void)
{
	struct nand_chip *this;

	// release resources, unregister device(s)
	nand_release(stb_77xx_nand_mtd);
	
	// release iomaps
	this = (struct nand_chip*) &stb_77xx_nand_mtd[1];
	iounmap((void *)this->IO_ADDR_R);

	// free the MTD device structure
	kfree(stb_77xx_nand_mtd);
	
	// deinit pio
	stb_77xx_nand_deinit_pio();
} // end of stb_77xx_nand_cleanup

module_exit(stb_77xx_nand_cleanup);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Michael Schenk michael.schenk@albistechnologies.com");
MODULE_DESCRIPTION("Glue layer for NAND flash on STB77xx board revision 1.8 09.04.2009");
