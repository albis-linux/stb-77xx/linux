// STB77xx NOR Flash

#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <asm/io.h>
#include <asm/errno.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>

#define SZ_64K				0x00010000
#define SZ_8K				0x00002000
#define ONBOARD_ADDR 		0x00000000
#define ONBOARD_SIZE 		(64 * SZ_64K)
#define ONBOARD_BANKWIDTH 	2
#define dprintk(x...)  		printk(KERN_DEBUG x)

static struct mtd_info* stb_77xx_nor_mtd;
static struct mtd_partition *parsed_parts;
static const char *probes[] = { "cmdlinepart", NULL };

static struct map_info stb_77xx_nor_map = {
	.name = "STB77xx 4MB NOR Flash",
	.size = ONBOARD_SIZE,
	.bankwidth = ONBOARD_BANKWIDTH,
};

static struct mtd_partition stb_77xx_nor_partitions[7] = {
	{
		.name = "NOR U-BOOT",
		.offset = 0x00000000,
		.size = (4 * SZ_64K)
	},
	{
		.name = "NOR Splash Screen",
		.offset = MTDPART_OFS_APPEND,
		.size = (2 * SZ_64K)
	},
	{
		.name = "NOR Kernel Main",
		.offset = MTDPART_OFS_APPEND,
		.size =  (29 * SZ_64K)
	},	
	{
		.name = "NOR Kernel Recovery",
		.offset = MTDPART_OFS_APPEND,
		.size = (18 * SZ_64K)
	},
	{
		.name = "NOR Rootdisk Recovery",
		.offset = MTDPART_OFS_APPEND,
		.size = (10 * SZ_64K)
	},
	{
		.name = "NOR Environment 1",
		.offset = MTDPART_OFS_APPEND,
		.size = (4 * SZ_8K)
	},			
	{
		.name = "NOR Environment 2",
		.offset = MTDPART_OFS_APPEND,
		.size = (4 * SZ_8K)
	}			
};

////////////////////////////////////////////////////////////////////////////////
// driver initialization routine
//
// PARAMETERS
//	NONE
//	
//	RETURN
// 		int (0 otherwise error code)
////////////////////////////////////////////////////////////////////////////////
int __init stb_77xx_nor_init(void)
{
	int nr_parts = 0;
	
	printk(KERN_NOTICE
	       "STB77xx onboard NOR flash device: 0x%08x (%d.%dMb) at 0x%08x\n",
	       ONBOARD_SIZE,
	       (ONBOARD_SIZE / (1024 * 1024)),
	       (ONBOARD_SIZE / ((1024 * 1024) / 10)) % 10, ONBOARD_ADDR);

	stb_77xx_nor_map.phys = ONBOARD_ADDR;
	stb_77xx_nor_map.size = ONBOARD_SIZE;
	stb_77xx_nor_map.virt = (unsigned long *)ioremap(stb_77xx_nor_map.phys, stb_77xx_nor_map.size);
	dprintk("%s %s[%d] stb_77xx_nor_map.virt = 0x%08x\n", __FILE__, __FUNCTION__,
			__LINE__, (int)stb_77xx_nor_map.virt);
	
	if(stb_77xx_nor_map.virt == 0) {
		printk(KERN_ERR "Failed to ioremap onboard Flash\n");
	} 
	else {
#ifndef CONFIG_MTD_COMPLEX_MAPPINGS
		simple_map_init(&stb_77xx_nor_map);
#endif
		stb_77xx_nor_mtd = do_map_probe("cfi_probe", &stb_77xx_nor_map);

		if (stb_77xx_nor_mtd != NULL) {
			stb_77xx_nor_mtd->owner = THIS_MODULE;
#ifdef CONFIG_MTD_CMDLINE_PARTS
			nr_parts = parse_mtd_partitions(stb_77xx_nor_mtd, probes,
											&parsed_parts, 0);
#endif
			if (nr_parts <= 0)
				add_mtd_partitions(stb_77xx_nor_mtd, stb_77xx_nor_partitions,
								   ARRAY_SIZE(stb_77xx_nor_partitions));
			else
				add_mtd_partitions(stb_77xx_nor_mtd, parsed_parts,
								   nr_parts);
		} 
		else {
			iounmap((void *)stb_77xx_nor_map.virt);
		}
	}

	return -ENXIO;
} // end of stb_77xx_nor_init

////////////////////////////////////////////////////////////////////////////////
// Clean up routine
//
// PARAMETERS
//	NONE
//	
//	RETURN
// 		NONE
////////////////////////////////////////////////////////////////////////////////
static void __exit stb_77xx_nor_cleanup(void)
{
	if (stb_77xx_nor_mtd) {
		del_mtd_partitions(stb_77xx_nor_mtd);
		map_destroy(stb_77xx_nor_mtd);
	}
	if (stb_77xx_nor_map.virt != 0) {
		iounmap((void *)stb_77xx_nor_map.virt);
		stb_77xx_nor_map.virt = 0;
	}
} // end of stb_77xx_nor_cleanup

module_init(stb_77xx_nor_init);
module_exit(stb_77xx_nor_cleanup);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Michael Schenk michael.schenk@siemens.com");
MODULE_DESCRIPTION("Glue layer for 4MB Spansion NOR flash on STB77xx board revision 1.6 18.06.2007");
